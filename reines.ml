(* Question 2.1 *)
let list_of_int n =
    List.init n (fun i -> i)

let print_list_int l =
    List.iter (fun x -> Printf.printf "%d " x) l


(* Question 2.2 *)
let succ_list l =
    List.map (fun x -> x+1) l 

let pred_list l =
    List.map (fun x -> x-1) l 


(* Question 2.3 *)
let diff l1 l2 =
    List.fold_left (fun acc x -> if not (List.exists (fun y -> x = y) l2 ) then x :: acc else acc) [] l1

(* Question 2.4 *)
let remove l x =
    List.fold_right ( fun e acc -> if e != x then e :: acc else acc ) l []

(* Question 2.5 *)
let rec nb_solutions a b c =
    match a with
    | [] -> 1
    | _ ->
        let possible = diff (diff a b) c in
        List.fold_left (fun acc x -> (nb_solutions (remove a x) (pred_list (x::b)) (succ_list (x::c))) + acc ) 0 possible

let reines n =
    nb_solutions (list_of_int n) [] []

let () = 
    let l = list_of_int 10 in
    let sl = succ_list l in
    let pl = pred_list l in
    print_list_int l;
    Printf.printf "\n";
    print_list_int sl;
    Printf.printf "\n";
    print_list_int pl;
    Printf.printf "\n";

    let diff1 = diff l sl in
    let diff2 = diff l pl in

    print_list_int diff1;
    Printf.printf "\n";
    print_list_int diff2;
    Printf.printf "\n";

    let l2 = list_of_int 10 in
    let l3 = remove l2 3 in

    print_list_int l3;
    Printf.printf "\n";

    Printf.printf "%d\n" (reines 10)