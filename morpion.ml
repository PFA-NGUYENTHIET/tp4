type marque = { c : bool; p : int * int }

type grille = marque list

(* Question 1.1 *)
let g1 = [
  { c = true; p = (0,0)};
  { c = true; p = (0,1)};
  { c = true; p = (0,2)};
  { c = false; p = (2,0)};
  { c = false; p = (1,1)};
  { c = false; p = (2,2)}
]

let g2 = [
  { c = true; p = (1,0)};
  { c = true; p = (0,1)};
  { c = true; p = (1,2)};
  { c = false; p = (2,0)};
  { c = false; p = (1,1)};
  { c = false; p = (0,2)}
]

let g3 = [
  { c = true; p = (1,0)};
  { c = true; p = (2,0)};
  { c = true; p = (0,1)};
  { c = true; p = (1,2)};
  { c = false; p = (0,0)};
  { c = false; p = (1,1)};
  { c = false; p = (2,1)};
  { c = false; p = (0,2)};
  { c = false; p = (2,2)}
]

let g4 = [
  { c = true; p = (0,0)};
  { c = true; p = (0,1)};
  { c = true; p = (0,2)}
]

(* Question 1.2 *)
let dans_les_bornes l = 
  List.for_all (fun {p = (x,y); _} -> x >= 0 && x <= 3 && y >= 0 && y <= 3) l

(* Question 1.3 *)
let existe_symbole g x y =
  List.exists (fun m -> fst (m.p) = x && snd (m.p) = y) g

(* Question 1.4 *)
let rec sans_doublons g =
  match g with
  | [] -> true
  | x :: s -> ( not ( existe_symbole s (fst x.p) (snd x.p) ) ) && sans_doublons s

(* Question 1.5 *)
let compter g =
  List.fold_left (fun (croix,rond) r -> if r.c then (croix + 1,rond) else (croix, rond + 1)) (0,0) g

(* Question 1.6 *)
let bonne_grille g =
  let a,b = compter g in
  dans_les_bornes g && sans_doublons g && (a >= b) && ((a-b) <= 1)

(* Question 1.7 *)
exception Grille_invalide

let bonne_grille_exn g =
  if bonne_grille g then
    () 
  else
    raise Grille_invalide

(* Question 1.8 *)
let gagne g = 
  let l = List.init 3 ( fun i -> (i,i) )
  :: List.init 3 ( fun i -> (2-i,i) )
  :: List.init 3 ( fun i -> List.init 3 ( fun j -> (i,j) ) )
  @ List.init 3 ( fun i -> List.init 3 ( fun j -> (j,i) ) )
  in

  List.exists (
    fun r -> List.for_all ( fun (x,y) -> existe_symbole g x y) r
  ) l

(* Question 1.9 *)
let extraire g b =
  List.filter (fun x -> x.c = b) g

(* Question 1.10 *)
let print_grille l =
    List.iter (fun { c = b; p = (x,y)} -> Printf.printf "c : %B, " b; Printf.printf "(%d,%d)\n" x y) l

let qui_gagne g =
  try
    bonne_grille_exn g;
    let croix = extraire g true in
    let rond = extraire g false in

    if gagne croix then
      Printf.printf "croix gagne\n"
    else
      if gagne rond then
        Printf.printf "rond gagne\n"
      else
        Printf.printf "match nul\n";
  with
    Grille_invalide -> Printf.printf "La grille est invalide\n";;


let () =
  Printf.printf "%B\n" (dans_les_bornes g1);
  Printf.printf "%B\n" (dans_les_bornes g2);
  Printf.printf "%B\n" (dans_les_bornes g3);
  Printf.printf "%B\n" (existe_symbole g1 1 0);
  Printf.printf "%B\n" (sans_doublons g1);
  Printf.printf "%B\n" (sans_doublons g2);
  Printf.printf "%B\n" (sans_doublons g3);

  let (a,b) = compter g1 in
  Printf.printf "%d - %d\n" a b;

  let (c,d) = compter g2 in
  Printf.printf "%d - %d\n" c d;

  let (e,f) = compter g3 in
  Printf.printf "%d - %d\n" e f;

  Printf.printf "%B\n" (bonne_grille g1);
  Printf.printf "%B\n" (bonne_grille g2);
  Printf.printf "%B\n" (bonne_grille g3);
  bonne_grille_exn g1;
  bonne_grille_exn g2;
  (* va lever une exception *)
  (* bonne_grille_exn g3; *)

  Printf.printf "%B\n" (gagne g1);
  Printf.printf "%B\n" (gagne g2);
  Printf.printf "%B\n" (gagne g3);
  Printf.printf "%B\n" (gagne g4);

  let g1bis = extraire g1 true in
  let g1bisbis = extraire g1 false in
  

  print_grille g1;
  Printf.printf "\n";
  print_grille g1bis;
  Printf.printf "\n";
  print_grille g1bisbis;

  qui_gagne g1;
  qui_gagne g2;
  qui_gagne g3;
  qui_gagne g4;


